jQuery(function() {
  $("a[rel~=popover], .has-popover").popover();
  $("a[rel~=tooltip], .has-tooltip").tooltip();
});

$(document).ready(function(){
  $('.phone_with_ddd').mask('(00) 00000-0000');
});