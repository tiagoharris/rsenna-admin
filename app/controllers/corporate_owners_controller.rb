class CorporateOwnersController < ApplicationController
  before_action :set_corporate_owner, only: [:show, :edit, :update, :destroy]

  # GET /corporate_owners
  # GET /corporate_owners.json
  def index
    @corporate_owners = CorporateOwner.all
  end

  # GET /corporate_owners/1
  # GET /corporate_owners/1.json
  def show
  end

  # GET /corporate_owners/new
  def new
    @corporate_owner = CorporateOwner.new
  end

  # GET /corporate_owners/1/edit
  def edit
  end

  # POST /corporate_owners
  # POST /corporate_owners.json
  def create
    @corporate_owner = CorporateOwner.new(corporate_owner_params)

    respond_to do |format|
      if @corporate_owner.save
        format.html { redirect_to corporate_owners_url, notice: 'Company owner was successfully created.' }
        format.json { render :show, status: :created, location: @corporate_owner }
      else
        format.html { render :new }
        format.json { render json: @corporate_owner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /corporate_owners/1
  # PATCH/PUT /corporate_owners/1.json
  def update
    respond_to do |format|
      if @corporate_owner.update(corporate_owner_params)
        format.html { redirect_to corporate_owners_url, notice: 'Company owner was successfully updated.' }
        format.json { render :show, status: :ok, location: @corporate_owner }
      else
        format.html { render :edit }
        format.json { render json: @corporate_owner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /corporate_owners/1
  # DELETE /corporate_owners/1.json
  def destroy
    @corporate_owner.destroy
    respond_to do |format|
      format.html { redirect_to corporate_owners_url, notice: 'Company owner was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_corporate_owner
      @corporate_owner = CorporateOwner.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def corporate_owner_params
      params.require(:corporate_owner).permit(:name, :contact_name, :contact_cell_phone)
    end
end
