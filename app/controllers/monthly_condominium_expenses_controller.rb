class MonthlyCondominiumExpensesController < ApplicationController
  before_action :set_monthly_condominium_expense, only: [:show, :edit, :update, :destroy]

  # GET /monthly_condominium_expenses
  # GET /monthly_condominium_expenses.json
  def index
    @monthly_condominium_expenses = MonthlyCondominiumExpense.all
  end

  # GET /monthly_condominium_expenses/1
  # GET /monthly_condominium_expenses/1.json
  def show
  end

  # GET /monthly_condominium_expenses/new
  def new
    @monthly_condominium_expense = MonthlyCondominiumExpense.new
  end

  # GET /monthly_condominium_expenses/1/edit
  def edit
  end

  # POST /monthly_condominium_expenses
  # POST /monthly_condominium_expenses.json
  def create
    @monthly_condominium_expense = MonthlyCondominiumExpense.new(monthly_condominium_expense_params)

    respond_to do |format|
      if @monthly_condominium_expense.save
        format.html { redirect_to monthly_condominium_expenses_url, notice: 'Monthly condominium expense was successfully created.' }
        format.json { render :show, status: :created, location: @monthly_condominium_expense }
      else
        format.html { render :new }
        format.json { render json: @monthly_condominium_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /monthly_condominium_expenses/1
  # PATCH/PUT /monthly_condominium_expenses/1.json
  def update
    respond_to do |format|
      if @monthly_condominium_expense.update(monthly_condominium_expense_params)
        format.html { redirect_to monthly_condominium_expenses_url, notice: 'Monthly condominium expense was successfully updated.' }
        format.json { render :show, status: :ok, location: @monthly_condominium_expense }
      else
        format.html { render :edit }
        format.json { render json: @monthly_condominium_expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /monthly_condominium_expenses/1
  # DELETE /monthly_condominium_expenses/1.json
  def destroy
    @monthly_condominium_expense.destroy
    respond_to do |format|
      format.html { redirect_to monthly_condominium_expenses_url, notice: 'Monthly condominium expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_monthly_condominium_expense
      @monthly_condominium_expense = MonthlyCondominiumExpense.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def monthly_condominium_expense_params
      params.require(:monthly_condominium_expense).permit(:due_date, :total, :total_paid, :total_remaining)
    end
end
