class Apartment < ApplicationRecord
	belongs_to :ownable, polymorphic: true
	belongs_to :renter, optional: true

	scope :occupied, -> { where(:ocuppied => true).pluck(:number).flatten }
	scope :numbers, -> { pluck(:number).flatten }

	# Use :to_global_id to populate the form
	def ownable_gid
	  ownable&.to_global_id
	end

	# Set the :bloggable from a Global ID (handles the form submission)
	def ownable_gid=(gid)
	  self.ownable = GlobalID::Locator.locate gid
	end
end
