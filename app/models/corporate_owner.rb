class CorporateOwner < ApplicationRecord
	has_many :apartments, :as => :ownable
	validates_presence_of :name, :contact_name, :contact_cell_phone
	validates_format_of :contact_cell_phone, :with => /(\(\d{2}\)\s)(\d{4,5}\-\d{4})/
end
