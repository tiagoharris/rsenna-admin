class MonthlyCondominiumExpenseItem < ApplicationRecord
  belongs_to :monthly_condominium_expense
  validates_presence_of :name, :description, :value
end
