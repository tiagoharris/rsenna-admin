class Owner < ApplicationRecord
	has_many :apartments, :as => :ownable
	validates_presence_of :name, :cell_phone
	validates_presence_of :spouse_name, :if => lambda{ |object| object.spouse_cell_phone.present? }
	validates_presence_of :spouse_cell_phone, :if => lambda{ |object| object.spouse_name.present? }
	validates_format_of :cell_phone, :with => /(\(\d{2}\)\s)(\d{4,5}\-\d{4})/
	validates_format_of :spouse_cell_phone, :with => /(\(\d{2}\)\s)(\d{4,5}\-\d{4})/, :if => lambda{ |object| object.spouse_cell_phone.present? }
end
