json.extract! apartment, :id, :number, :floor, :created_at, :updated_at
json.url apartment_url(apartment, format: :json)
