json.extract! corporate_owner, :id, :name, :contact_name, :contact_cell_phone, :created_at, :updated_at
json.url corporate_owner_url(corporate_owner, format: :json)
