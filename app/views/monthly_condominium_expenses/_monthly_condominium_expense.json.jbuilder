json.extract! monthly_condominium_expense, :id, :due_date, :total, :total_paid, :total_remaining, :created_at, :updated_at
json.url monthly_condominium_expense_url(monthly_condominium_expense, format: :json)
