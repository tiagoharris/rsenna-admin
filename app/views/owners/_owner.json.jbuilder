json.extract! owner, :id, :name, :spouse_name, :cell_phone, :spouse_cell_phone, :created_at, :updated_at
json.url owner_url(owner, format: :json)
