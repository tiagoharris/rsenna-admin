json.extract! renter, :id, :name, :spouse_name, :cell_phone, :spouse_cell_phone, :created_at, :updated_at
json.url renter_url(renter, format: :json)
