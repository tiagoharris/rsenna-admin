Rails.application.routes.draw do
  resources :monthly_condominium_expenses
  resources :corporate_owners
  resources :owners
  resources :renters
  root 'home#index'
  resources :apartments
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
