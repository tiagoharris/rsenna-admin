class CreateApartments < ActiveRecord::Migration[6.0]
  def change
    create_table :apartments do |t|
      t.integer :number
      t.integer :floor
      t.bigint  :renter_id
      t.boolean :ocuppied, :default => false

      t.timestamps
    end
    add_reference :apartments, :ownable, polymorphic: true, index: true
  end
end
