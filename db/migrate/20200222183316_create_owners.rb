class CreateOwners < ActiveRecord::Migration[6.0]
  def change
    create_table :owners do |t|
      t.string :name
      t.string :spouse_name
      t.string :cell_phone
      t.string :spouse_cell_phone

      t.timestamps
    end
  end
end
