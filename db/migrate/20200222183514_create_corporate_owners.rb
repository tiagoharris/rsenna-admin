class CreateCorporateOwners < ActiveRecord::Migration[6.0]
  def change
    create_table :corporate_owners do |t|
      t.string :name
      t.string :contact_name
      t.string :contact_cell_phone

      t.timestamps
    end
  end
end
