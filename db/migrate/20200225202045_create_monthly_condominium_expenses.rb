class CreateMonthlyCondominiumExpenses < ActiveRecord::Migration[6.0]
  def change
    create_table :monthly_condominium_expenses do |t|
      t.date :due_date
      t.float :total
      t.float :total_paid
      t.float :total_remaining

      t.timestamps
    end
  end
end
