class CreateMonthlyCondominiumExpenseItems < ActiveRecord::Migration[6.0]
  def change
    create_table :monthly_condominium_expense_items do |t|
      t.references :monthly_condominium_expense, null: false, foreign_key: true, index: { name: :mce_idx }
      t.string :name
      t.string :description
      t.float :value

      t.timestamps
    end
  end
end
