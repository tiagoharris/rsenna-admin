# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_02_25_202143) do

  create_table "apartments", force: :cascade do |t|
    t.integer "number"
    t.integer "floor"
    t.bigint "renter_id"
    t.boolean "ocuppied", default: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "ownable_type"
    t.integer "ownable_id"
    t.index ["ownable_type", "ownable_id"], name: "index_apartments_on_ownable_type_and_ownable_id"
  end

  create_table "corporate_owners", force: :cascade do |t|
    t.string "name"
    t.string "contact_name"
    t.string "contact_cell_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "monthly_condominium_expense_items", force: :cascade do |t|
    t.integer "monthly_condominium_expense_id", null: false
    t.string "name"
    t.string "description"
    t.float "value"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["monthly_condominium_expense_id"], name: "mce_idx"
  end

  create_table "monthly_condominium_expenses", force: :cascade do |t|
    t.date "due_date"
    t.float "total"
    t.float "total_paid"
    t.float "total_remaining"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "owners", force: :cascade do |t|
    t.string "name"
    t.string "spouse_name"
    t.string "cell_phone"
    t.string "spouse_cell_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "renters", force: :cascade do |t|
    t.string "name"
    t.string "spouse_name"
    t.string "cell_phone"
    t.string "spouse_cell_phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "monthly_condominium_expense_items", "monthly_condominium_expenses"
end
