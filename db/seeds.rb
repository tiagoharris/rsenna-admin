corporate_owner = CorporateOwner.create!(name: "OGV Construtora Eireli", contact_name: "Odair José Silva Rodrigues", contact_cell_phone: "(37) 99102-0443")

apartments = [
  [ 101, 1, corporate_owner ],
  [ 102, 1, corporate_owner ],
  [ 103, 1, corporate_owner ],
  [ 201, 2, corporate_owner ],
  [ 202, 2, corporate_owner ],
  [ 203, 2, corporate_owner ],
  [ 301, 3, corporate_owner ],
  [ 302, 3, corporate_owner ],
  [ 303, 3, corporate_owner ],
]

apartments.each do |number, floor, corporate_owner|
	Apartment.create!(number: number, floor: floor, ownable: corporate_owner)
end 