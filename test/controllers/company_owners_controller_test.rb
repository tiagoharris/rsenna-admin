require 'test_helper'

class CorporateOwnersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @corporate_owner = corporate_owners(:one)
  end

  test "should get index" do
    get corporate_owners_url
    assert_response :success
  end

  test "should get new" do
    get new_corporate_owner_url
    assert_response :success
  end

  test "should create corporate_owner" do
    assert_difference('CorporateOwner.count') do
      post corporate_owners_url, params: { corporate_owner: { contact_cell_phone: @corporate_owner.contact_cell_phone, contact_name: @corporate_owner.contact_name, name: @corporate_owner.name } }
    end

    assert_redirected_to corporate_owner_url(CorporateOwner.last)
  end

  test "should show corporate_owner" do
    get corporate_owner_url(@corporate_owner)
    assert_response :success
  end

  test "should get edit" do
    get edit_corporate_owner_url(@corporate_owner)
    assert_response :success
  end

  test "should update corporate_owner" do
    patch corporate_owner_url(@corporate_owner), params: { corporate_owner: { contact_cell_phone: @corporate_owner.contact_cell_phone, contact_name: @corporate_owner.contact_name, name: @corporate_owner.name } }
    assert_redirected_to corporate_owner_url(@corporate_owner)
  end

  test "should destroy corporate_owner" do
    assert_difference('CorporateOwner.count', -1) do
      delete corporate_owner_url(@corporate_owner)
    end

    assert_redirected_to corporate_owners_url
  end
end
