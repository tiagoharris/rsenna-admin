require 'test_helper'

class MonthlyCondominiumExpensesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @monthly_condominium_expense = monthly_condominium_expenses(:one)
  end

  test "should get index" do
    get monthly_condominium_expenses_url
    assert_response :success
  end

  test "should get new" do
    get new_monthly_condominium_expense_url
    assert_response :success
  end

  test "should create monthly_condominium_expense" do
    assert_difference('MonthlyCondominiumExpense.count') do
      post monthly_condominium_expenses_url, params: { monthly_condominium_expense: { due_date: @monthly_condominium_expense.due_date, total: @monthly_condominium_expense.total, total_paid: @monthly_condominium_expense.total_paid, total_remaining: @monthly_condominium_expense.total_remaining } }
    end

    assert_redirected_to monthly_condominium_expense_url(MonthlyCondominiumExpense.last)
  end

  test "should show monthly_condominium_expense" do
    get monthly_condominium_expense_url(@monthly_condominium_expense)
    assert_response :success
  end

  test "should get edit" do
    get edit_monthly_condominium_expense_url(@monthly_condominium_expense)
    assert_response :success
  end

  test "should update monthly_condominium_expense" do
    patch monthly_condominium_expense_url(@monthly_condominium_expense), params: { monthly_condominium_expense: { due_date: @monthly_condominium_expense.due_date, total: @monthly_condominium_expense.total, total_paid: @monthly_condominium_expense.total_paid, total_remaining: @monthly_condominium_expense.total_remaining } }
    assert_redirected_to monthly_condominium_expense_url(@monthly_condominium_expense)
  end

  test "should destroy monthly_condominium_expense" do
    assert_difference('MonthlyCondominiumExpense.count', -1) do
      delete monthly_condominium_expense_url(@monthly_condominium_expense)
    end

    assert_redirected_to monthly_condominium_expenses_url
  end
end
