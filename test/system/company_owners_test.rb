require "application_system_test_case"

class CorporateOwnersTest < ApplicationSystemTestCase
  setup do
    @corporate_owner = corporate_owners(:one)
  end

  test "visiting the index" do
    visit corporate_owners_url
    assert_selector "h1", text: "Company Owners"
  end

  test "creating a Company owner" do
    visit corporate_owners_url
    click_on "New Company Owner"

    fill_in "Contact cell phone", with: @corporate_owner.contact_cell_phone
    fill_in "Contact name", with: @corporate_owner.contact_name
    fill_in "Name", with: @corporate_owner.name
    click_on "Create Company owner"

    assert_text "Company owner was successfully created"
    click_on "Back"
  end

  test "updating a Company owner" do
    visit corporate_owners_url
    click_on "Edit", match: :first

    fill_in "Contact cell phone", with: @corporate_owner.contact_cell_phone
    fill_in "Contact name", with: @corporate_owner.contact_name
    fill_in "Name", with: @corporate_owner.name
    click_on "Update Company owner"

    assert_text "Company owner was successfully updated"
    click_on "Back"
  end

  test "destroying a Company owner" do
    visit corporate_owners_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Company owner was successfully destroyed"
  end
end
