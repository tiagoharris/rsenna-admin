require "application_system_test_case"

class MonthlyCondominiumExpensesTest < ApplicationSystemTestCase
  setup do
    @monthly_condominium_expense = monthly_condominium_expenses(:one)
  end

  test "visiting the index" do
    visit monthly_condominium_expenses_url
    assert_selector "h1", text: "Monthly Condominium Expenses"
  end

  test "creating a Monthly condominium expense" do
    visit monthly_condominium_expenses_url
    click_on "New Monthly Condominium Expense"

    fill_in "Due date", with: @monthly_condominium_expense.due_date
    fill_in "Total", with: @monthly_condominium_expense.total
    fill_in "Total paid", with: @monthly_condominium_expense.total_paid
    fill_in "Total remaining", with: @monthly_condominium_expense.total_remaining
    click_on "Create Monthly condominium expense"

    assert_text "Monthly condominium expense was successfully created"
    click_on "Back"
  end

  test "updating a Monthly condominium expense" do
    visit monthly_condominium_expenses_url
    click_on "Edit", match: :first

    fill_in "Due date", with: @monthly_condominium_expense.due_date
    fill_in "Total", with: @monthly_condominium_expense.total
    fill_in "Total paid", with: @monthly_condominium_expense.total_paid
    fill_in "Total remaining", with: @monthly_condominium_expense.total_remaining
    click_on "Update Monthly condominium expense"

    assert_text "Monthly condominium expense was successfully updated"
    click_on "Back"
  end

  test "destroying a Monthly condominium expense" do
    visit monthly_condominium_expenses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Monthly condominium expense was successfully destroyed"
  end
end
