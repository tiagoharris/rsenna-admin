require "application_system_test_case"

class RentersTest < ApplicationSystemTestCase
  setup do
    @renter = renters(:one)
  end

  test "visiting the index" do
    visit renters_url
    assert_selector "h1", text: "Renters"
  end

  test "creating a Renter" do
    visit renters_url
    click_on "New Renter"

    fill_in "Cell phone", with: @renter.cell_phone
    fill_in "Name", with: @renter.name
    fill_in "Spouse cell phone", with: @renter.spouse_cell_phone
    fill_in "Spouse name", with: @renter.spouse_name
    click_on "Create Renter"

    assert_text "Renter was successfully created"
    click_on "Back"
  end

  test "updating a Renter" do
    visit renters_url
    click_on "Edit", match: :first

    fill_in "Cell phone", with: @renter.cell_phone
    fill_in "Name", with: @renter.name
    fill_in "Spouse cell phone", with: @renter.spouse_cell_phone
    fill_in "Spouse name", with: @renter.spouse_name
    click_on "Update Renter"

    assert_text "Renter was successfully updated"
    click_on "Back"
  end

  test "destroying a Renter" do
    visit renters_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Renter was successfully destroyed"
  end
end
